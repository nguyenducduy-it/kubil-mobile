import appSettings from 'tns-core-modules/application-settings';

export const set = (name: string, value: string) => {
  return appSettings.setString(name, value);
}

// return '' if not found
export const get = (name: string) => {
  return appSettings.getString(name, '');
}

// return false if not exist
export const has = (name: string) => {
  return appSettings.hasKey(name);
}
