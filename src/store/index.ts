import Vue from 'nativescript-vue';
import Vuex from 'vuex';

import Test from './modules/test';
import Counter from './modules/counter';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {},
  modules: {
    test: Test,
    counter: Counter
  }
});

Vue.prototype.$store = store;

export default store;
