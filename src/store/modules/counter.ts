import { Action, Mutation, Module, VuexModule } from 'vuex-module-decorators';

@Module({
  namespaced: true,
  name: 'counter'
})
export default class Counter extends VuexModule {
  counter: number = 0;

  @Mutation INCREMENT() {
    this.counter++;
  }

  @Mutation DECREMENT() {
    this.counter--;
  }

  @Action({ commit: 'INCREMENT' }) increase() { }

  @Action({ commit: 'DECREMENT' }) decrease() { }
}
