import { Action, Mutation, MutationAction, Module, VuexModule } from 'vuex-module-decorators';
import fetch from '@/helper/fetch';

@Module({
  namespaced: true,
  name: 'test'
})
export default class Test extends VuexModule {
  data: string = '';

  @MutationAction({ mutate: ['data'] })
  async get_sample() {
    const response = await fetch({
      url: '/hello',
      method: 'GET'
    });

    return response;
  }
}
