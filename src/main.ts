import Vue from 'nativescript-vue';
import router from './router';
import store from './store/index';
import './assets/styles.scss';

Vue.config.silent = true;

new Vue({
  router,
  store
}).$start();
