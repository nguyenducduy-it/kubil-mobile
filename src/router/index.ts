import Vue from 'nativescript-vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import TestPage from '../pages/Test.vue';

const router = new VueRouter({
  pageRouting: true,
  routes: [
    {
      path: '/test',
      component: TestPage,
      meta: {
        title: 'Test'
      }
    },
    { path: '*', redirect: '/test' }
  ]
});

router.replace('/test');

export default router;
